//DOM's
const loanButtonElement = document.getElementById("loans-button");
const repayLoanButtonElement = document.getElementById("repay-button");
const balanceNumberElement = document.getElementById("balance");
const debtNumberElement = document.getElementById("debt-number");
const debtTextElement = document.getElementById("debt-text");
const bankButtonElement = document.getElementById("bank-button");
const workButtonElement = document.getElementById("work-button");
const salaryNumberElement = document.getElementById("salary-number");
const laptopListElement = document.getElementById("laptop-title");
const laptopSpecsElement = document.getElementById("laptop-specs")
const laptopDescriptionElement= document.getElementById("laptop-description");
const laptopNameElement = document.getElementById("laptop-name");   
const laptopImageElement = document.getElementById("image");
const buyPriceElement = document.getElementById("buy-price");
const buyButtonElement = document.getElementById("buy-button");



//variables
let startBalance = 100;
let bankAccount = 0;
let debt = 0;
let isLoaning = false;
let salary = 0;
const wage = 100;
const imageDefaultURL ="https://noroff-komputer-store-api.herokuapp.com/";
let currentLaptopPrice = 0;
let currentLaptopName = "";
const computerArray = []

//initialise
updateBalance(startBalance);
updateSalary(0)
updateDebt(0);
//set initial visibility
debtNumberElement.style.visibility = "hidden";
repayLoanButtonElement.style.visibility = "hidden";
debtTextElement.style.visibility = "hidden";

//fetch request
const computers = async () => {

  fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then((response) => response.json())
    .then((data) => {
      computerArray.push(...data);
       computerArray.forEach(element => addComputerToList(element)); 
       updatePCInfoInit();
    })   
};    
//activate fetch request
computers();



const addComputerToList = (comp) => {
    const computerElement = document.createElement("option")
    computerElement.value = comp.id;
    computerElement.appendChild(document.createTextNode(comp.title));
    laptopListElement.appendChild(computerElement)

}    





//event listener


repayLoanButtonElement.addEventListener("click", () => {
    if (debt === 0) {
        alert("You have no more to repay! 🤩");
        return 0;
      }
    //input
    let amountToPayBack = prompt("How much do you want to pay back? MAX: " + debt +" NOK")
    if(amountToPayBack<0){
      alert("Again, this is not a charity, you have to pay us!")
      return 0;
    }

    if (isNaN(amountToPayBack)) 
    {
    alert("Please input a number between 0 NOk and " + bankAccount + " NOK!")
    }
    else repayDebt(amountToPayBack)

    if(debt <= 0)
    {
        debtNumberElement.style.visibility = "hidden";
        repayLoanButtonElement.style.visibility = "hidden";
        debtTextElement.style.visibility = "hidden";
    }
}
);

workButtonElement.addEventListener("click", ()=>{
    salary += wage;
    salaryNumberElement.innerText = salary;
    alert("Generating money⚙⚙⚙")
})

bankButtonElement.addEventListener("click",()=>{
    if(debt > 0)
    {
        let repayDebtFromSalary = parseInt(salary*0.1)
        let profitSalary = parseInt(salary-repayDebtFromSalary);
        repayDebtFromWork(repayDebtFromSalary,profitSalary);
        updateSalary(-salary);
    }
    else if(debt === 0 )
    {
      updateBalance(salary)
      updateSalary(-salary);
    }
})

buyButtonElement.addEventListener("click",()=>{
  if(currentLaptopPrice <= bankAccount){
    updateBalance(-currentLaptopPrice);
    alert(`You bought ${currentLaptopName}! Congratulations`)
  }
  else{
    alert("Could not finish transaction!")
  }
})

loanButtonElement.addEventListener("click", () => {
  if (debt !== 0) {
    alert("You need to repay the loan BEFORE you can loan anymore!");
    return 0;
  }

  let loanAmmount = parseInt(prompt("Get a loan today! Terms and conditions may applie.", "0"));
  if (isNaN(loanAmmount)) {
    alert(
      "Please input a number between 0 NOk and " + 2 * bankAccount + " NOK!"
    );
  } 
  else if (loanAmmount > 2 * bankAccount) {
    alert(
      "Insufficient funds! " +
        "Please input a number between 0 NOk and " +
        2 * bankAccount +
        " NOK!"
    );
  } 
  else if (loanAmmount <= 0) 
  {
    alert("This is not a  charity 🤡! You loan from us, not the other way around! 🤬 ");
  } else if (loanAmmount > 0 && loanAmmount <= bankAccount * 2) {
    debtNumberElement.style.visibility = "visible";
    repayLoanButtonElement.style.visibility = "visible";
    debtTextElement.style.visibility = "visible";
    updateBalance(loanAmmount);
    updateDebt(loanAmmount)
    alert("You loaned" + loanAmmount + " NOK, outstanding debt is " + debt+ " NOK")
  }
});

laptopListElement.addEventListener("change",updatePCInfo );

//Functions
function updateBalance(moneyToAdd) {
  bankAccount += parseInt(moneyToAdd);
  balanceNumberElement.innerText = parseInt(bankAccount) + " NOK";
}

function updateDebt(moneyToAdd) {
    debt += parseInt(moneyToAdd);
    debtNumberElement.innerText = parseInt(debt) + " NOK";
}

function updateSalary(deltaSalary){
    salary += parseInt(deltaSalary)
    salaryNumberElement.innerText = parseInt(salary) + " NOK"
}


  function repayDebt(amountToPayBack){
    if(amountToPayBack > debt )
    {
        let tempdebt = debt;
        updateBalance(-debt)
        updateDebt(-debt)
        alert("You have repayed " + tempdebt + " NOK, outstanding debt is " + debt+ " NOK")
        
    }
    else if(amountToPayBack <= bankAccount){

        updateBalance(-amountToPayBack)
        updateDebt(-amountToPayBack);
        alert("You have repayed " + amountToPayBack + " NOK, outstanding debt is " + debt+ " NOK");
    }

    if(debt <= 0)
    {
        debtNumberElement.style.visibility = "hidden";
        repayLoanButtonElement.style.visibility = "hidden";
        debtTextElement.style.visibility = "hidden";
    }
  }

  function repayDebtFromWork(amountToPayBack, repaySalary){
    if(amountToPayBack > debt )
    {
        let tempdebt = debt;
        updateBalance(repaySalary)
        updateDebt(-debt)
        alert("You have repayed " + tempdebt + " NOK, salary amount: " + repaySalary+ " NOK")
        
    }
    else if(amountToPayBack <= bankAccount){

        updateBalance(repaySalary)
        updateDebt(-amountToPayBack);
        alert("You have repayed " + amountToPayBack + " NOK, salary amount: " + repaySalary+ " NOK");
    }

    if(debt <= 0)
    {
        debtNumberElement.style.visibility = "hidden";
        repayLoanButtonElement.style.visibility = "hidden";
        debtTextElement.style.visibility = "hidden";
    }
  }


//update functions
function updatePCInfo(event){

  const selectedPc = computerArray[event.target.selectedIndex];
  laptopSpecsElement.innerText = selectedPc.specs.join("\n")
  laptopDescriptionElement.innerText = selectedPc.description;
  laptopNameElement.innerText = selectedPc.title
  laptopImageElement.src = imageDefaultURL + selectedPc.image;
  buyPriceElement.innerHTML = selectedPc.price + " NOK";
  currentLaptopPrice = selectedPc.price;
  currentLaptopName = selectedPc.title;
}
//initialize pc info
function updatePCInfoInit(){

  const selectedPc = computerArray[0];
  laptopSpecsElement.innerText = selectedPc.specs.join("\n")
  laptopDescriptionElement.innerText = selectedPc.description;
  laptopNameElement.innerText = selectedPc.title
  laptopImageElement.src = imageDefaultURL + selectedPc.image;
  laptopImageElement.alt = selectedPc.title;
  buyPriceElement.innerHTML = selectedPc.price + " NOK";
  currentLaptopPrice = selectedPc.price;
  currentLaptopName = selectedPc.title;
}